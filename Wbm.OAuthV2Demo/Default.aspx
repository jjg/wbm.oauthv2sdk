﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="Wbm.OAuthV2SDK.OAuths" %>
<%@ Import Namespace="Wbm.OAuthV2SDK.Helpers" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>我不忙-微博OAuth2SDK演示V3.1.0324</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <%--声明：
        本Demo只实现了登录的简单功能，更多复杂的功能请根据自身需求进行增减。
        使用本SDK请勿脱离各平台官方API文档。
        如使用过程发生错误或不明白之处，请加入Q群：25844867 开发者之家
    --%>
    <div>
        <h2>
            我不忙-微博OAuth2SDK演示V3.1.0324
        </h2>
        <p>
            官方论坛：http://wobumang.com/afx
        </p>
        <%--
            我不忙-微博OAuth2SDK 使用流程：
            1、根据需要求修改配置文件(Wbm.OAuthV2.config)。
            2、验证认证信息缓存。(参考Default.aspx文件)
            3、获取用户认证地址。(参考Login.aspx文件)
            4、获取/缓存认证信息。(参考RedirectUri.aspx文件)
            5、获取用户资源。(参考Default.aspx文件)
        --%>
        <%
            /*
             * 使用方法：
             * 1、实例化应用，my_app为配置文件自定义应用名称，查看Wbm.OAuthV2.config文件
             *    var oauth = Wbm.OAuthV2SDK.OAuths.OAuthBase.CreateInstance();
             *    或
             *    var oath = new Wbm.OAuthV2SDK.OAuths.Sinas.SinaOAuth();
             *    
             * 
             * 2、验证认证信息缓存
             *     if (oauth!=null && oauth.HasCache){}
             *     
             * 
             * 3、获取认证参数
             *     var paras = oauth.GetTokenParas();
             *     
             * 
             * 4、请求API地址，users_show为配置文件API名称，查看Wbm.OAuthV2.config文件
             *     var json = oauth.ApiByHttpGet("users_show", paras);
             *     
             *     返回类结果
             *     var user = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Sinas.Models.SinaMUser>(oauth.ApiByHttpGet("users_show", paras_2));
             *     string name = user.nick;
             *     
             *     返回字典结果
             *     var dic = UtilHelper.ParseJson<Wbm.OAuthV2SDK.Entitys.ADictionary<string, object>>(oauth.ApiByHttpGet(url, paras_1));
             *     string name = dic["text"];
             *     
             *     验证API是否错误，ret不等于0时发生错误。
             *     if (user.ret == 0){}
             *     或
             *     if (user.error_code == 0){}
             *     
             * 
             * 5、显示结果
             */
        %>
        <%
            //获取当前已登录的协议
            var oauth = Wbm.OAuthV2SDK.OAuths.OAuthBase.CreateInstance();
            if (oauth != null && oauth.HasCache)
            {//判断是否已登录
                string oauth_name = oauth.OAuthName;
        %>
        <p>
            <a href="Logout.aspx">退出</a>
        </p>
        <h4>
            单平台演示 (当前登录：<%=oauth.OAuthDesc%>)</h4>
        <hr />
        <%           
                var sb = new StringBuilder();
                try
                {
        %>
        <%
    if (oauth_name == "sina")
    { //新浪微博
        /*关注列表 直接返回JSON*/
        //var paras_1 = oauth.GetTokenParas();
        //paras_1.Add("source_id", "1714799145");
        //paras_1.Add("target_id", "1770104142");
        //var json = oauth.ApiByHttpGet("friendships_show", paras_1);
        //sb.AppendLine("<p>");
        //sb.AppendLine("json数据：<br /><textarea>" + json + "</textarea>");
        //sb.AppendLine("</p>");

        /*获取用户模型 直接返回JSON*/
        string uid = "1770104142";
        var paras_2 = oauth.GetTokenParas();
        paras_2.Add("uid", uid);
        var response = oauth.ApiByHttpGet("users_show", paras_2);
        var user = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Sinas.Models.SinaMUser>(response);
        //验证API是否错误
        if (user.error_code == 0)
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息：</strong><br /><img src=\"" + user.profile_image_url + "\" style=\"vertical-align: middle\" /> " + user.name + "");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p style=\"color: red;\">");
            sb.AppendLine("<strong>用户信息错误：</strong><br />" + user.error_description + "(" + user.error + ")");
            sb.AppendLine("</p>");
        }

        //获取微博列表
        var paras_3 = oauth.GetTokenParas();
        paras_3.Add("uid", uid);
        paras_3.Add("count", "5");
        var list = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Sinas.Models.SinaMStatusList>(oauth.ApiByHttpGet("statuses_user_timeline", paras_3));
        //验证API是否错误
        if (list.error_code == 0)
        {
            sb.AppendLine("<div>");
            sb.AppendLine("<strong>获取微博列表：</strong><br />");
            sb.AppendLine("<ul>");
            foreach (var item in list.statuses)
            {
                sb.AppendLine("<li>" + item.text + "</li>");
            }
            sb.AppendLine("</ul>");
            sb.AppendLine("</div>");
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>获取微博列表错误：</strong><br />" + list.error + "");
            sb.AppendLine("</p>");
        }
    }%>
        <%
    else if (oauth_name == "tenc")
    {//腾讯微博
        //获取json数据
        //var paras_1 = oauth.GetTokenParas();
        //paras_1.Add("format", "json");
        //var json = oauth.ApiByHttpGet("user_info", paras_1);
        //sb.AppendLine("<p>");
        //sb.AppendLine("json数据：<br /><textarea>" + json + "</textarea>");
        //sb.AppendLine("</p>");

        //获取用户模型
        var paras_2 = oauth.GetTokenParas();
        var user_data = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Tencs.Models.TencMUserData>(oauth.ApiByHttpGet("user_info", paras_2));
        //验证API是否错误
        if (user_data.ret == 0)
        {
            var user = user_data.data;
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息：</strong><br /><img src=\"" + user.head + "/50\" style=\"vertical-align: middle\" /> " + user.nick + "");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息错误：</strong><br />" + user_data.msg + "");
            sb.AppendLine("</p>");
        }

        //获取微博模型
        var paras_3 = oauth.GetTokenParas();
        paras_3.Add("format", "json");
        paras_3.Add("id", "96288093290462");
        var t_data = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Tencs.Models.TencMTweetData>(oauth.ApiByHttpGet("t_show", paras_3));
        //验证API是否错误
        if (t_data.ret == 0)
        {
            var tweet = t_data.data;
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>获取单条微博：</strong><br />微博id：" + tweet.id + "<br /> " + tweet.text + "");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p style=\"color: red;\">");
            sb.AppendLine("<strong>获取单条微博错误：</strong><br />" + t_data.errcode + "(" + t_data.msg + ")");
            sb.AppendLine("</p>");
        }

    }%>
        <%
    else if (oauth_name == "neasy")
    {//网易微博
        //获取json数据
        //var paras = oauth.GetTokenParas();
        //var json = oauth.ApiByHttpGet("users_show", paras);
        //sb.AppendLine("<p>");
        //sb.AppendLine("json数据：<br /><textarea>" + json + "</textarea>");
        //sb.AppendLine("</p>");


        //获取用户模型
        var paras2 = oauth.GetTokenParas();
        var user = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Neasys.Models.NeasyMUser>(oauth.ApiByHttpGet("users_show", paras2));
        //验证API是否错误
        if (user.error_code == 0)
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息：</strong><br /><img src=\"" + user.profile_image_url + "\" style=\"vertical-align: middle\" /> " + user.name + "");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息错误：</strong><br />" + user.error + "");
            sb.AppendLine("</p>");
        }

        //获取微博模型
        var paras_1 = oauth.GetTokenParas();
        //===注意：此处的URL需要做特殊处理====
        var api = oauth.ApiUrl("statuses_show_id");
        var url = string.Format(api, "412268425912833457");
        //===注意：此处的URL需要做特殊处理====
        var status = UtilHelper.ParseJson<Wbm.OAuthV2SDK.Entitys.ADictionary<string, object>>(oauth.ApiUrlByHttpGet(url, paras_1));
        //验证API是否错误
        if (status.ContainsKey("error_code") == false)
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>获取单条微博：</strong><br />" + status["id"] + "<br /> " + status["text"] + "");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>获取单条微博错误：</strong><br />" + status["error"] + "");
            sb.AppendLine("</p>");
        }

        //获取微博列表
        var paras_2 = oauth.GetTokenParas();
        paras_2.Add("count", "5");
        var list = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Neasys.Models.NeasyMStatus[]>(oauth.ApiByHttpGet("statuses_home_timeline", paras_2));
        //验证API是否错误
        if (list[0].error_code == 0)
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>获取微博列表：</strong><br />");
            sb.AppendLine("<ul>");
            foreach (var item in list)
            {
                sb.AppendLine("<li>" + item.text + "</li>");
            }
            sb.AppendLine("</ul>");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>获取微博列表错误：</strong><br />" + list[0].error + "");
            sb.AppendLine("</p>");
        }
    }%>
        <%
    else if (oauth_name == "qzone")
    {//QQ空间
        //获取json数据
        var paras_1 = oauth.GetTokenParas();
        var json = oauth.ApiByHttpGet("user_get_user_info", paras_1);
        sb.AppendLine("<p>");
        sb.AppendLine("json数据：<br /><textarea>" + json + "</textarea>");
        sb.AppendLine("</p>");


        //获取用户模型
        var paras_2 = oauth.GetTokenParas();
        var user = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Qzones.Models.QzoneMUser>(oauth.ApiByHttpGet("user_get_user_info", paras_2));
        //验证API是否错误
        if (user.ret == 0)
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息：</strong><br /><img src=\"" + user.figureurl_1 + "\" style=\"vertical-align: middle\" /> " + user.nickname + "");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息错误：</strong><br />" + user.msg + "");
            sb.AppendLine("</p>");
        }
    }
        %>
        <%
    else if (oauth_name == "renren")
    {//人人网
        //获取json数据
        //var paras_1 = oauth.GetTokenParas();
        //paras_1.Add("method", "users.getInfo");
        //paras_1.Add("format", "json");
        //var json = oauth.ApiByHttpGet("restserver", paras_1);
        //sb.AppendLine("<p>");
        //sb.AppendLine("json数据：<br /><textarea>" + json + "</textarea>");
        //sb.AppendLine("</p>");

        //获取用户模型
        var uid = "446154186";
        var paras_2 = oauth.GetTokenParas();
        paras_2.Add("userId", uid);
        var result = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Renrens.Models.RenrenMRsUser>(oauth.ApiByHttpGet("user_get", paras_2));
        //验证API是否错误
        if (result.error == null)
        {
            var user = result.response;
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息：</strong><br /><img src=\"" + user.avatar[0].url + "\" style=\"vertical-align: middle\" /> " + user.name + "");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息错误：<br>" + result.error.message + "</strong>");
            sb.AppendLine("</p>");
        }

        //获取状态列表
        var paras_3 = oauth.GetTokenParas();
        paras_3.Add("ownerId", uid);
        paras_3.Add("pageSize", "20");
        paras_3.Add("pageNumber", "1");
        var response = oauth.ApiByHttpGet("status_list", paras_3);
        var result_3 = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Renrens.Models.RenrenMRsStatusList>(response);
        //验证API是否错误
        if (result_3.error == null)
        {
            var statusList = result_3.response;
            if (statusList.Length > 0)
            {
                foreach (var status in statusList)
                {
                    sb.AppendLine("<p>");
                    sb.AppendLine("<strong>状态内容：</strong><br />" + status.content + "");
                    sb.AppendLine("</p>");
                }
            }
            else
            {
                sb.AppendLine("<p>");
                sb.AppendLine("<strong>状态内容：</strong><br />暂无状态");
                sb.AppendLine("</p>");
            }
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>状态内容错误：<br>" + result_3.error.message + "</strong>");
            sb.AppendLine("</p>");
        }
    }
        %>
        <%
    else if (oauth_name == "kaixin")
    {//开心网
        //获取json数据
        var uid = "138894080";
        //var paras = oauth.GetTokenParas();
        //paras.Add("uids", uid);
        //var json = oauth.ApiByHttpGet("users_show", paras);
        //sb.AppendLine("<p>");
        //sb.AppendLine("json数据：<br /><textarea>" + json + "</textarea>");
        //sb.AppendLine("</p>");


        //获取用户模型
        var paras2 = oauth.GetTokenParas();
        paras2.Add("uids", uid);
        var json2 = oauth.ApiByHttpGet("users_show", paras2);
        var userList = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Kaixins.Models.KaixinMUserList>(json2);
        //验证API是否错误
        if (userList.error_code == 0)
        {
            if (userList.users.Length > 0)
            {
                var user = userList.users[0];
                sb.AppendLine("<p>");
                sb.AppendLine("<strong>用户信息：</strong><br /><img src=\"" + user.logo50 + "\" style=\"vertical-align: middle\" /> " + user.name + "");
                sb.AppendLine("</p>");
            }
            else
            {
                sb.AppendLine("<p>");
                sb.AppendLine("<strong>用户信息：</strong><br />暂无相关用户");
                sb.AppendLine("</p>");
            }
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息错误：</strong><br />" + userList.error + "");
            sb.AppendLine("</p>");
        }

        //获取微博模型
        var paras_3 = oauth.GetTokenParas();
        paras_3.Add("start", "0");
        paras_3.Add("num", "20");
        paras_3.Add("category", "0");
        var json3 = oauth.ApiByHttpGet("records_me", paras_3);
        var list = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Kaixins.Models.KaixinMRecordList>(json3);
        //验证API是否错误
        if (list.error_code == 0)
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>获取微博列表：</strong><br />");
            sb.AppendLine("<ul>");
            foreach (var item in list.data)
            {
                sb.AppendLine("<li>" + item.main.content + "</li>");
            }
            sb.AppendLine("</ul>");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>获取微博列表错误：</strong><br />" + list.error + "");
            sb.AppendLine("</p>");
        }
    }
        %>
        <%
    else if (oauth_name == "vdisk")
    {
        //新浪微盘
        /*关注列表 直接返回JSON*/
        var paras_1 = oauth.GetTokenParas();
        var json = oauth.ApiByHttpGet("account_info", paras_1);
        sb.AppendLine("<p>");
        sb.AppendLine("json数据：<br /><textarea>" + json + "</textarea>");
        sb.AppendLine("</p>");

        /*获取用户模型 直接返回JSON*/
        var paras_2 = oauth.GetTokenParas();
        var response = oauth.ApiByHttpGet("account_info", paras_2);
        var user = UtilHelper.ParseJson<Wbm.OAuthV2SDK.OAuths.Vdisks.Models.VdiskMInfo>(response);
        //验证API是否错误
        if (user.error_code == 0)
        {
            sb.AppendLine("<p>");
            sb.AppendLine("<strong>用户信息：</strong><br /><img src=\"" + user.profile_image_url + "\" style=\"vertical-align: middle\" /> " + user.user_name + "");
            sb.AppendLine("</p>");
        }
        else
        {
            sb.AppendLine("<p style=\"color: red;\">");
            sb.AppendLine("<strong>用户信息错误：</strong><br />" + user.error + "(" + user.error_code + ")");
            sb.AppendLine("</p>");
        }
    }%>
        <%
}
catch (Exception ex)
{
    sb.Append(ex.Message);
}
        %>
        <div>
            <%=sb.ToString() %>
        </div>
        <p>
            <strong>发送微博：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            <input type="text" name="text" value="今天我使用了#我不忙-微博OAuth2SDK#成功发表微博，非常开心啦！" />
            <input type="hidden" name="act" value="0" />
            <input type="submit" value="发表" />
            </form>
        </p>
        <p>
            <strong>发送带图片微博：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            <input type="text" name="text" value="今天我使用了#我不忙-微博OAuth2SDK#成功发表图片微博，非常激动啦！" />
            <input type="hidden" name="act" value="1" />
            <input type="submit" value="发表" />
            </form>
            <br />
            <strong>示例图片：</strong>
            <br />
            <img src="Upload.jpg" alt="示例图片" style="border: 1px solid #ccc;" />
        </p>
        <div style="height: 100px;">
        </div>
        <h4>
            多平台演示</h4>
        <hr />
        <div class="list">
            <%foreach (var item in Wbm.OAuthV2SDK.OAuthConfig.GetConfigOAuths())
              {
                  //模拟数据库获取accessToken
                  var token = Session[item.name];
            %>
            <label class="ico <%=item.name %>">
                <span></span>
                <%=item.desc %>
                <br />
                <%if (token != null)
                  { %>
                <label>
                    已绑定</label>
                <%}
                  else
                  { %>
                <a href="login.aspx?oauth=<%=item.name %>">绑定</a>
                <%} %>
            </label>
            <%} %>
        </div>
        <div class="clear">
        </div>
        <p>
            <strong>多平台发发送微博：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            <input type="text" name="text" value="今天我使用了#我不忙-微博OAuth2SDK#成功发表多平台微博，非常开心啦！" />
            <input type="hidden" name="act" value="3" />
            <input type="submit" value="发表" />
            </form>
        </p>
        <p>
            <strong>多平台发送带图片微博：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            <input type="text" name="text" value="今天我使用了#我不忙-微博OAuth2SDK#成功发表多平台图片微博，非常激动啦！" />
            <input type="hidden" name="act" value="4" />
            <input type="submit" value="发表" />
            </form>
            <br />
            <strong>示例图片：</strong>
            <br />
            <img src="Upload.jpg" alt="示例图片" style="border: 1px solid #ccc;" />
        </p>
        <%}
            else
            { %>
        <div class="list">
            <%foreach (var item in Wbm.OAuthV2SDK.OAuthConfig.GetConfigOAuths())
              { %>
            <a href="login.aspx?oauth=<%=item.name %>" class="ico <%=item.name %>"><span></span>
                <%=item.desc %></a>
            <%} %>
            <div class="clear">
            </div>
        </div>
        <%} %>
    </div>
</body>
</html>
