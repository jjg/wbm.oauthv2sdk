﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" %>

<%@ Import Namespace="Wbm.OAuthV2SDK.OAuths" %>
<%@ Import Namespace="Wbm.OAuthV2SDK.Entitys" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>发送微博 - 我不忙-微博OAuth2SDK演示V3.1.0324</title>
</head>
<body>
    <div>
        <h2>
            我不忙-微博OAuth2SDK演示V3.1.0324
        </h2>
        <p>
            官方论坛：http://wobumang.com
        </p>
        <p>
            <%     
                string msg = Request["msg"];
                string act = Request["act"];
                string text = Request["text"];

                if (act == "100")
                {
                    Response.Write(msg);
                }
                else
                {
                    if (act == "3" || act == "4")
                    {
                        if (act == "4")
                        {
                            //发图片微博
                            var file = Server.MapPath("Upload.jpg");
                            var result = Wbm.OAuthV2Demo.Controllers.MoreOAuth.SendStatusWithPic(text, file);
                            msg = "发送图片微博";
                            if (!string.IsNullOrEmpty(result))
                            {
                                msg += result;
                            }
                            else
                            {
                                msg += "，发送成功";
                            }
                        }
                        else
                        {
                            var result = Wbm.OAuthV2Demo.Controllers.MoreOAuth.SendStatus(text);
                            msg = "发送微博";
                            if (!string.IsNullOrEmpty(result))
                            {
                                msg += result;
                            }
                            else
                            {
                                msg += "，发送成功";
                            }
                        }
                        Response.Redirect("post.aspx?act=100&msg=" + Server.UrlEncode(msg));
                        Response.End();
                    }
                    else
                    {
                        if (OAuthBase.HasCacheOAuth)
                        {
                            var oauth = OAuthBase.CreateInstance();
                            if (oauth != null)
                            {
                                if (oauth.HasCache)
                                {
                                    try
                                    {
                                        ApiResult result = null;
                                        if (act == "1")
                                        {
                                            //发图片微博
                                            var file = Server.MapPath("Upload.jpg");
                                            result = oauth.SendStatusWithPic(text, file);
                                            msg = "发送图片微博";
                                        }
                                        else
                                        {
                                            /*方法一:调用内置方法*/
                                            result = oauth.SendStatus(text);
                                            msg = "发送微博";
                                        }
                                        if (result.ret == 0)
                                        { //发送成功
                                            msg += "，发送成功";
                                        }
                                        else
                                        { //发送失败
                                            msg += "，发送失败！" + result.msg + "(" + result.errcode + ")";
                                        }

                                        /* 方法二:直接调用接口
                                        NameValueCollection paras = oauth.GetTokenParas();
                                        paras.Add("status", text);
                                        string response = oauth.ApiByHttpPost("statuses_update", paras);
                                        */

                                    }
                                    catch (Exception ex)
                                    {
                                        Response.Write(ex);
                                        Response.End();
                                    }
                                    Response.Redirect("post.aspx?act=100&msg=" + Server.UrlEncode(msg));
                                    Response.End();
                                }
                                else
                                {
                                    Response.Write("发送失败，找不到相对就的token");
                                    Response.End();
                                }

                            }
                            else
                            {
                                Response.Write("发送失败，找不到相对应的接口");
                                Response.End();
                            }
                        }
                        else
                        {
                            Response.Write("发送失败，找不到相对应的缓存");
                        }
                    }
                }
            %>
        </p>
    </div>
</body>
</html>
