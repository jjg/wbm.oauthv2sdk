﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="Wbm.OAuthV2SDK.OAuths" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>退出应用 - 我不忙-微博OAuth2SDK演示V3.1.0324</title>
</head>
<body>
    <div>
        <%            
            //退出应用
            var oauth = OAuthBase.CreateInstance();
            if (oauth != null)
            {
                oauth.ClearCache();
            }
            Response.Redirect("./");
        %>
    </div>
</body>
</html>
