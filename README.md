我不忙-微博OAuthV2SDK
--------------------
### 一个更方便，更快速的C#SDK，希望能帮助更多的朋友学习和使用 

> 官网：http://wobumang.com/afx 我不忙爱分享<br />
> 微群：http://q.weibo.com/152956 开发者之家<br />
> Q 群：25844867 开发者之家<br />

> 代号：Wbm.OAuthV2SDK<br />
> 版本：V3.0.0811<br />
> 作者：xusion<br />
> 发布：2013-08-11<br />

### 官方wiki
[http://afx.wobumang.com/afx/oauth/wbm.oauthv2sdk](http://afx.wobumang.com/afx/oauth/wbm.oauthv2sdk)<br />

### 下载地址
1.[查看附件](http://git.oschina.net/wobumang/wbm.oauthv2sdk/attach_files)<br />
2.[查看标签列表](https://git.oschina.net/wobumang/wbm.oauthv2sdk/repository/tags)<br />

### 问题反馈
如果您是使用者，发现了我们的程序有bug，或有更好的建议，在请[issues](http://git.oschina.net/wobumang/wbm.oauthv2sdk/issues)里提交。<br />
如果您是开发者，可以[Fock](http://git.oschina.net/wobumang/wbm.oauthv2sdk/fork)我们的程序，再把您的修改或改进提交到Pull Requests，我们要认真审核的。