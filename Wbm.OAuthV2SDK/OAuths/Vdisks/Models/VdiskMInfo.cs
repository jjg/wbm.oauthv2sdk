﻿using System;
namespace Wbm.OAuthV2SDK.OAuths.Vdisks.Models
{
    /// <summary>
    /// 实体类MUsers 。
    /// </summary>
    [Serializable]
    public class VdiskMInfo : VdiskMError
    {
        /// <summary>
        /// 微盘用户ID 
        /// </summary>
        public string uid { set; get; }
        /// <summary>
        /// 微博用户ID
        /// </summary>
        public VdiskMQuotaInfo quota_info { set; get; }
        /// <summary>
        /// 用户昵称
        /// </summary>
        public string screen_name { set; get; }
        /// <summary>
        /// 友好显示名称
        /// </summary>
        public string user_name { set; get; }
        /// <summary>
        /// 用户所在地区
        /// </summary>
        public string location { set; get; }
        /// <summary>
        /// 用户头像地址，50×50像素
        /// </summary>
        public string profile_image_url { set; get; }
        /// <summary>
        /// 是否是微博认证用户，即加V用户，true：是，false：否
        /// </summary>
        public bool verified { set; get; }
        /// <summary>
        /// 用户大头像地址
        /// </summary>
        public string avatar_large { set; get; }
        /// <summary>
        /// 性别，m：男、f：女、n：未知
        /// </summary>
        public string gender { set; get; }

    }

    /// <summary>
    /// 实体类可用空间
    /// </summary>
    [Serializable]
    public class VdiskMQuotaInfo
    {
        /// <summary>
        /// 可用空间
        /// </summary>
        public string quota { set; get; }
        /// <summary>
        /// consumed
        /// </summary>
        public string consumed { set; get; }
    }
}
/*
 * Author: xusion
 * Created: 2012.04.10
 * Support: http://wobumang.com
 */